'use strict';

const Controller = require('egg').Controller;

class NspController extends Controller {
  async message(msg) {
    const { app, ctx } = this;
    const nsp = app.io.of('/');
    const message = ctx.args[0] || {};
    console.log('收到socket.io消息:', message);

    try {
      if (!msg) return;
      // console.log('TCL: NspController -> exchange -> msg', msg);
      nsp.emit('data', { name: 'tang', age: 999666, info: '中文测试' });
    } catch (error) {
      app.logger.error(error);
    }
  }
}

module.exports = NspController;
