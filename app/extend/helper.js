'use strict';

exports.success = (ctx, res = {}) => {
  // const { ctx } = this;

  if (!res.msg) res.msg = '请求成功';

  ctx.body = {
    ret: 0,
    data: res,
  };
};

exports.validateQueryParams = (querys, validateArry) => {
  const { ctx } = this;
  const isContain = (arr1, arr2) => {
    for (let i = arr2.length - 1; i >= 0; i--) {
      if (!arr1.includes(arr2[i])) {
        return false;
      }
    }
    return true;
  };

  const validate = isContain(validateArry, querys);

  if (!validate) {
    const info = {
      msg: '请求参数不合法',
      details: {
        request: querys,
        validate: validateArry,
      },
    };
    ctx.throw(422, info);
  }
};

