/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-19 15:50:02
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-11-08 14:30:21
 */
'use strict';
/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller, io } = app;
  router.get('/', controller.home.index); // 首页
  router.get('/ws', controller.home.indexWs); // websocket首页
  router.get('/getSocketMsg', controller.home.getSocketMsg); // ws测试url

  // 17x测试
  router.get('/17x/fixed', controller.specialTestNarrow.fixedMeasuring); // 17x定频
  router.get('/17x/hop', controller.specialTestNarrow.hopMeasuring); // 17x跳频
  // console.log('TCL: controller', controller);

  // 窄带通用测试
  router.get('/narrowband/general', controller.generalTestNarrow.generalTest_narrow); //


  // socket.io
  io.of('/').route('message', io.controller.nsp.message);
  // ws('/hello', ws.controller.hello);

  // 校准表文件上传
  router.post('/upload/tx', controller.uploadJson.Upload_file_tx);
  router.post('/upload/rx', controller.uploadJson.Upload_file_rx);
  // router.resources('file', '/upload', controller.uploadJson.Upload_fileController);
  // router.post('file',  controller.uploadJson.Upload_fileController);
  // demo 演示控制接口
  router.get('/17x/fixed/demo', controller.demonstrate.demoCfg);

  // 自检接口
  router.get('/selfCheck/adda', controller.selfCheck.adda);
};
