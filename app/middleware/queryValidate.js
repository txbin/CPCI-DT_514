/*
 * @Description: 检测请求参数合法性的中间件，根据路由URL地址自动去
 * 匹配app\t_extend\request\validateDict.js路径中字典值
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-20 14:54:28
 * @LastEditors: TangXB
 * @LastEditTime: 2019-08-21 15:27:43
 */
'use strict';
module.exports = options => {
  return async function queryValidate(ctx, next) {
    const isContain = (sourceArry, arr2) => {
      for (let i = arr2.length - 1; i >= 0; i--) {
        if (!sourceArry.includes(arr2[i])) {
          return false;
        }
      }
      return true;
    };

    const queryArr = ctx.query.params.split(',');// 获取请求测试数组
    const testName = ctx.request.url.split('?')[0].slice(1);// 获取路由地址
    const validate = isContain(options[testName], queryArr);

    if (!validate) {
      const info = {
        ret: -1,
        msg: '请求参数不合法',
        details: {
          request: queryArr,
          validate: options[testName],
        },
      };
      ctx.app.emit('error', info, ctx);
      ctx.body = info;
      return;
    }
    await next();
  };
};
