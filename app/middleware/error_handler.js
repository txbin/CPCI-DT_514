/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-20 10:15:34
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-11-07 15:17:05
 */
'use strict';
module.exports = () => {
  return async function errorHandler(ctx, next) {
    try {
      await next();
    } catch (err) {
      ctx.app.emit('error', err, ctx);
      const status = err.status || 500;
      // const error =
      //   status === 500 && ctx.app.config.env === 'prod'
      //     ? '服务器异常'
      //     : err.message;
      // const error = err.message;

      // 从 error 对象上读出各个属性，设置到响应中
      // ctx.body = { error };
      if (status === 422) {
        ctx.body = {};
        ctx.body.ret = -1;
        ctx.body.data = {};
        ctx.body.data.msg = err.message;
      }

      if (status === 423) {
        ctx.body = {};
        ctx.body.ret = -1;
        ctx.body.msg = err.message || '硬件控制错误';
      }

      if (status === 424) {
        ctx.body = {};
        // console.log('TCL: errorHandler -> ctx.body', ctx.body);
        ctx.body.ret = -1;
        ctx.body.data = {};
        ctx.body.data.msg = err || '服务器处理错误';
      }
      ctx.status = status;
    }
  };
};
