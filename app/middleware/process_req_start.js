/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-08-21 15:28:13
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-10-12 21:36:05
 */
'use strict';
module.exports = options => {
  return async function processReqStart(ctx, next) {
    // 这里是通过设置中间件中的标志变量达到限制只能发起一个请求的功能
    if (options.isProcessReq) return ctx.helper.success(ctx, { msg: '现在正在处理之前测量请求，请稍后发起测试' });

    options.isProcessReq = true;
    await next();
    options.isProcessReq = false;

  };
};
