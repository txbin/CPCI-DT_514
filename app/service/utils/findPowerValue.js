/*
 * @Description:
 * @version:
 * @Author: tangxiaobin
 * @Date: 2019-06-24 15:43:17
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-10-24 09:20:57
 */
'use strict';
const FileIO = require('./FileIO');

const arryTools = require('./arryOption');

// function getTxValue(powerValue, freqValue, lookupFilePath) {
//   console.log('TCL: getTxValue -> powerValue, freqValue, lookupFilePath', { powerValue, freqValue, lookupFilePath });
//   return readFileAndFind(lookupFilePath, powerValue, freqValue);
// }
/**
 *获取实际的发射功率dbm值，因为u3上报的是衰减db单位，需要显示为dbm
 *
 * @param {number} [readPower] 读取链路的测量值
 * @param {Number} freqValue 频率值
 * @param {string} lookupFilePathRec 查找表路径
 * @return {Number} 校正值
 */
function getRxValue(readPower, freqValue, lookupFilePathRec) {
  const POWER_TABLES = FileIO.readTextFile(lookupFilePathRec, true);

  let freqKey = parseInt(freqValue);
  // FIXME: 此处手动校准表，因此使用的是统一30M查表，后面需要取消下面一行
  freqKey = 30;
  const findObjCatalogArry = POWER_TABLES.arr.filter(item => item.freq === freqKey);
  console.log('TCL: getRxValue -> findObjCatalogArry', findObjCatalogArry);

  const findObj = arryTools.findApproxValueInJsonArry(findObjCatalogArry, 'readPower', readPower);

  let actualPower = '';
  if (findObj) { actualPower = findObj.power; }
  return actualPower;
  // const valueIndex = _.findIndex(findObjCatalogArry, o => o.dut.atti === atti);

  // if (valueIndex === -1) return 0;

  // const compensationValue = findObjCatalogArry[valueIndex].power;
  // return compensationValue;
}

/**
 *根据设置功率值查找对应的内部、外部衰减值，并转换为8byte buffer对象
 *
 * @param {*} powerValue 设置功率值
 * @param {*} freqValue  对应查找表的频点值
 * @param {*} lookupFilePath 查找表文件路径
 * @return {object} json
 */
function getTxValue(powerValue, freqValue, lookupFilePath) {
  const POWER_TABLES = FileIO.readTextFile(lookupFilePath, true);

  const freqKey = parseInt(freqValue) + 'MHz';
  const findObj = arryTools.findApproxValueInJsonArry(
    POWER_TABLES[freqKey].reverse(),
    'power',
    powerValue
  );
  console.log('TCL: getTxValue -> POWER_TABLES[freqKey]', POWER_TABLES[freqKey].length);

  console.log('发射功率查找表结果：', freqKey, findObj);

  return findObj;
}

module.exports = {
  getTxValue,
  getRxValue,
};

// const path = require('path');
// const reviseTableDir = 'app/public/reviseTable';

// const nb_power_table_tx_file = path.join(reviseTableDir, 'nb_tx_revise_table.json');
// const nb_power_table_rx_file = path.join(reviseTableDir, 'nb_rx_revise_table.json');

// const carrpower = getTxValue(-81, 46, nb_power_table_tx_file);
// console.log('TCL: carrpower', carrpower);

// const recPower = getRxValue(-64, 35, nb_power_table_rx_file);
// console.log('TCL: recPower', recPower);
