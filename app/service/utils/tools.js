'use strict';
const sleep = time => {
  return new Promise(resolve => setTimeout(resolve, time));
};
/**
 *比较发送数据与接收数据是否一样
 *
 * @param {*} sendArry
 * @param {*} recArry
 * @return
 */
const _compareRecEqualSend = (sendArry, recArry) => {
  if (!Buffer.isBuffer(recArry)) {
    console.warn('接收不为buffer', recArry);
    return false;
  }

  const isEqual = recArry.equals(Buffer.from(sendArry));
  return isEqual;
};

exports.sleep = sleep;
exports._compareRecEqualSend = _compareRecEqualSend;
