'use strict';

/**
 *查找数组中与指定值最接近的数值,数组值只能从小到大排序
 *把要比较的这个数push到那个数组里面去，然后sort排序，从小到大or从大到小都可以，检测这个数和前面一个数以及和后面一个数绝对值差哪个小，小的那个就是最接近的数
 *
 * @param {*} arry 数组
 * @param {*} value 查找值
 * @return {*} 接近值对象
 */
function findApproValue(arry, value) {
  const newArry = [ ...arry ];
  newArry.push(value);
  newArry.sort((a, b) => a - b);

  const pushIndex = newArry.indexOf(value);

  const frontSubtractAbs = Math.abs(value - newArry[pushIndex - 1]);
  const behindSubtractAbs = Math.abs(value - newArry[pushIndex + 1]);

  const closestValueIndex = frontSubtractAbs < behindSubtractAbs ? pushIndex - 1 : pushIndex;
  // // console.log('TCL: findApproValue -> closestValueIndex', closestValueIndex);

  return closestValueIndex;
}

/**
 *查找json对象中，指定key值索引下，与指定值最接近json对象
 *
 * @param {*} jsonArry 数组
 * @param {*} sortKey 排序索引key
 * @param {*} value 查找值
 * @return {*} 接近值
 */
function findApproxValueInJsonArry(jsonArry, sortKey, value) {
  // // console.log('TCL: findApproxValueInJsonArry ->  sortKey, value', jsonArry[0], { sortKey, value });
  if (!jsonArry) {
    console.error('json数组查找表为空:');
    return;
  }
  // console.log('TCL: findApproxValueInJsonArry -> jsonArry', jsonArry.length);

  const sortArry = jsonArry.map(item => item[sortKey]);
  // // console.log('TCL: findApproxValueInJsonArry -> sortArry', sortArry.length);
  // console.log('TCL: findApproxValueInJsonArry -> jsonArry', jsonArry.length);


  const index = findApproValue(sortArry, value);
  // console.log('TCL: findApproxValueInJsonArry -> jsonArry', jsonArry.length);


  const obj = jsonArry[index];
  // console.log('TCL: findApproxValueInJsonArry -> jsonArry', jsonArry.length);
  // // console.log('TCL: findApproxValueInJsonArry -> obj', obj);
  console.log(`对象查找表索引:${index} value：${JSON.stringify(obj)}`);
  return obj;
}

module.exports = {
  findApproValue,
  findApproxValueInJsonArry,
};
