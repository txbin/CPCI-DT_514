'use strict';
const fs = require('fs');

const FileIO = {

  /**
 *
 * @param {*} absPath 文件绝对路径
 * @param { Boolean } isConvertJson  布尔值，是否转换为json对象
 */
  readTextFile: (absPath, isConvertJson) => {
    try {
      let bin = fs.readFileSync(absPath);
      if (bin[0] === 0xEF && bin[1] === 0xBB && bin[2] === 0xBF) {
        bin = bin.slice(3);
      }
      const str = bin.toString();
      const back = isConvertJson ? JSON.parse(str) : str;
      return back;
    } catch (error) {
      console.error(error);
      return null;
    }
  },
};

module.exports = FileIO;
