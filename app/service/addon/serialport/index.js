/*
 * @Description:so串口控制类
 * @version:
 * @Author: TangXB
 * @Date: 2019-07-18 11:49:39
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-10-12 21:26:58
 */

'use strict';

const ffi = require('ffi');
const ArrayType = require('ref-array');
const ref = require('ref');

const int = ref.types.int;
const IntArray = ArrayType(int);

const libZitSp = ffi.Library('libzitsp.so.1.0.0', {
  zitsp_create: [ 'pointer', [ 'string', 'int', 'pointer', 'pointer' ]],
  zitsp_delete: [ 'void', [ 'pointer' ]],
  zitsp_write: [ 'void', [ 'pointer', IntArray, 'int' ]],
});

class Serialport {
  constructor(COM, baudRate, onDataFunc, errorFunc = Buffer.alloc(0)) {
    this._spHandle = libZitSp.zitsp_create(
      COM,
      baudRate,
      onDataFunc,
      errorFunc
    );
  }
  close() {
    console.log('释放串口资源' + new Date().toLocaleTimeString());

    libZitSp.zitsp_delete(this._spHandle);
    this._spHandle = null;
    console.log('释放串口资源完成' + new Date().toLocaleTimeString());
  }

  write(array) {
    libZitSp.zitsp_write(this._spHandle, array, array.length);
  }

  /**
   *读取串口数据
   *
   * @param {Int8Array} array 装载接收数据的数组
   * @memberof Serialport
   */
  read(array) {
    libZitSp.zitsp_read(this._spHandle, array, array.length);
  }
}
module.exports = Serialport;
