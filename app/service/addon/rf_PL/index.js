/*
 * @Description:射频板控制
 * @version:
 * @Author: TangXB
 * @Date: 2019-07-08 10:21:16
 * @LastEditors  : TangXB
 * @LastEditTime : 2020-01-03 20:27:59
 */
'use strict';

const ffi = require('ffi');
// const ref = require('ref');

// const unint32_t = ref.types.uint32;

const libzitrf = ffi.Library('libzitrf.so.1.0.0', {
  zitrf_clk_init: [ 'int', []],
  zitrf_create: [ 'pointer', [ 'string', 'pointer' ]],

  zitrf_set_fqt: [ 'int', [ 'pointer', 'int' ]], // 发射频点  Mhz
  zitrf_set_fqr: [ 'int', [ 'pointer', 'int' ]], // 接收频点  Mhz

  zitrf_set_gn_rx: [ 'int', [ 'pointer', 'int' ]], // 接收增益 0~31.5db

  zitrf_set_gn_tx: [ 'void', [ 'pointer', 'double' ]], // 设置发射增益  0~31.5db 发射增益调节le1
  zitrf_set_at_tx: [ 'void', [ 'pointer', 'double' ]], // 设置发射衰减  0~31.5db 发射衰减调节le2
  zitrf_set_dig_at: [ 'void', [ 'pointer', 'int' ]], // 设置发射数字衰减  0 - 5  依次衰减6db  max -36db

  zitrf_set_lp: [ 'void', [ 'pointer', 'int' ]], // 设置收发模式  0 收发回环	1 接收模式	2 保留	3 发射模式
  zitrf_set_en_tx: [ 'void', [ 'pointer', 'int' ]], // 设置发射使能

  zitrf_set_sm: [ 'void', [ 'pointer', 'int' ]], // 通道选择:	0 小信号通道	1 大信号通道 (10W)

  zitrf_src_sel: [ 'void', [ 'pointer', 'int' ]], // 发送源选择; 0 :单音输出(30000),即输出为NCO频率; 1:FM输出; 2:输出0 ; 3:输出0
  zitrf_set_amp_tx: [ 'void', [ 'pointer', 'int' ]], // 发射放大选择  频率单位Mhz
  zitrf_set_dig_en: [ 'void', [ 'pointer', 'int' ]], // 发送源选择; 0 :单音输出(30000),即输出为NCO频率; 1:FM输出; 2:输出0 ; 3:输出0

  zitrf_get_detector_pwr: [ 'double', [ 'pointer' ]], // 回读功率检波器
  zitrf_get_temp: [ 'double', [ 'pointer' ]], // DS18B20 温度

  zitrf_set_pwr_tx: [ 'void', [ 'pointer', 'double' ]], // 直接设置发射功率，so层做查找校准表
  // zitrf_get_fqt: [ 'int', []],
  // zitrf_get_fqr: [ 'int', []],
  // zitrf_get_gn_rx: [ 'int', []],
  // zitrf_get_at_tx: [ 'int', []],
  // zitrf_get_en_tx: [ 'int', []],
  // zitrf_get_en_rx: [ 'int', []],
});

class rfBoardCfgAddon {
  constructor(params = '/dev/AxiGP0', callback = Buffer.alloc(0)) {
    this.HANDLE = libzitrf.zitrf_create(params, callback);
    libzitrf.zitrf_clk_init(); // 射频上电初始化
  }
  /**
   *设置收发使能
   *
   * @param {number} mode 0:收发回环，1：接收使能，3：发射使能
   * @memberof rfBoardCfgAddon
   */
  enRxTx(mode) {
    console.log('设置收发射频收发使能：', mode);
    libzitrf.zitrf_set_lp(this.HANDLE, mode);

    if (mode === 3) { // 如果是发射，还需要配置使能发射
      // libzitrf.zitrf_set_en_tx(this.HANDLE, 1);
    }

  }
  /**
   *设置发射衰减值
   *
   * @param {*} inAttGn gn硬件
   * @param {*} inAttTx tx衰减
   * @param {*} dig_att 数字衰减
   * @memberof rfBoardCfgAddon
   */
  setAttx(inAttGn, inAttTx, dig_att) {
    libzitrf.zitrf_set_gn_tx(this.HANDLE, inAttGn);
    libzitrf.zitrf_set_at_tx(this.HANDLE, inAttTx);
    libzitrf.zitrf_set_dig_at(this.HANDLE, dig_att);
  }

  /**
   *设置发射频点，number小数
   *
   * @param {number} fqt 发射频点
   * @memberof rfBoardCfgAddon
   */
  setFqt(fqt) {
    libzitrf.zitrf_set_fqt(this.HANDLE, fqt);
  }
  setFqr(fqr) {
    libzitrf.zitrf_set_fqr(this.HANDLE, fqr);
  }

  setMode(mode) {
    libzitrf.zitrf_src_sel(this.HANDLE, mode);
  }
  setPowerChannel(channel) {
    libzitrf.zitrf_set_sm(this.HANDLE, channel);
  }

  rf_tx_test_init() {
    console.log('射频发射初始化');

    this.enRxTx(3);
    this.setPowerChannel(1);
    libzitrf.zitrf_set_at_tx(this.HANDLE, 0);
    libzitrf.zitrf_set_gn_tx(this.HANDLE, 0);
    libzitrf.zitrf_set_dig_at(this.HANDLE, 0);

    libzitrf.zitrf_set_amp_tx(this.HANDLE, 0x0A); // 暂时固定不变

    libzitrf.zitrf_set_gn_rx(this.HANDLE, 0x0); // 接收暂时不考虑
    libzitrf.zitrf_set_fqr(this.HANDLE, 0b01001); // 接收时不考虑

    libzitrf.zitrf_set_fqt(this.HANDLE, 70);
    libzitrf.zitrf_src_sel(this.HANDLE, 0);

  }
  rf_rx_test_init() {
    this.enRxTx(1);
    libzitrf.zitrf_set_sm(this.HANDLE, 0x1);
    libzitrf.zitrf_set_at_tx(this.HANDLE, 0x0);
    libzitrf.zitrf_set_gn_tx(this.HANDLE, 0x0);
    libzitrf.zitrf_set_gn_rx(this.HANDLE, 0);
    libzitrf.zitrf_set_fqt(this.HANDLE, 50);
    libzitrf.zitrf_set_amp_tx(this.HANDLE, 0x0A);
    libzitrf.zitrf_set_fqr(this.HANDLE, 0b01001);
  }
  att1(value) {
    libzitrf.zitrf_set_gn_tx(this.HANDLE, value);
  }
  att2(value) {
    libzitrf.zitrf_set_at_tx(this.HANDLE, value);
  }
  digitAttx(value) {
    libzitrf.zitrf_set_dig_at(this.HANDLE, value);
  }
  gnRx(value) {
    libzitrf.zitrf_set_gn_rx(this.HANDLE, value);
  }
  /**
   *直接设置发射功率，so层做查找校准表
   *
   * @param {*} txPower
   * @memberof rfBoardCfgAddon
   */
  setTxPower(txPower) {
    libzitrf.zitrf_set_pwr_tx(this.HANDLE, txPower);
  }
  /**
   *功率检波器值
   *
   * @return {number} double
   * @memberof rfBoardCfgAddon
   */
  getDetectors() {
    return libzitrf.zitrf_get_detector_pwr(this.HANDLE);
  }
}

const _rfConfig_PLAddon = new rfBoardCfgAddon(); // 射频配置

module.exports = _rfConfig_PLAddon;
