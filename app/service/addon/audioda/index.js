'use strict';

const ffi = require('ffi');

const libzitauda = ffi.Library('libzitauda.so.1.0.0', {
  zitauda_create: [ 'pointer', [ 'string', 'pointer' ]],
  zitauda_delete: [ 'void', [ 'pointer' ]],
  zitauda_set_level: [ 'void', [ 'pointer', 'int' ]], // 设置幅度
  zitauda_set_frq: [ 'void', [ 'pointer', 'int' ]], // 设置频率
  zitauda_set_en: [ 'int', [ 'pointer', 'int' ]], // 使能播放
  // zitauda_get_en: [ 'int', [ 'int' ]],
  // zitauda_get_config: [ 'int', [ 'int' ]],
});

class AudioAddonDA {
  constructor(params = '/dev/AxiGP0', callback = Buffer.alloc(0)) {
    this._audaHandle = libzitauda.zitauda_create(params, callback);
    // libzitauda.zitauda_set_level(level);
    // libzitauda.zitauda_set_frq(frq);
  }
  setLevel(level) {
    libzitauda.zitauda_set_level(this._audaHandle, level);
  }

  setFreq(freq) {
    libzitauda.zitauda_set_frq(this._audaHandle, freq);
  }

  setEn(en) {
    libzitauda.zitauda_set_en(this._audaHandle, en);
  }

  release() {
    libzitauda.zitauda_delete(this._audaHandle);
  }
}
// const b = new AudioAddonDA();
// b.setEn(1);
// b.setFreq(1000);
// b.setLevel(210);

module.exports = AudioAddonDA;
