/*
 * @Description:设备控制
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-24 14:58:48
 * @LastEditors: TangXB
 * @LastEditTime: 2019-09-10 10:10:42
 */
'use strict';

const ffi = require('ffi');

const libZitDevAddon = ffi.Library('libzitdev.so.1.0.0', {
  zitdev_create: [ 'pointer', [ 'string', 'pointer' ]],
  zitdev_delete: [ 'void', [ 'pointer' ]],
  zitdev_set_en_ptt: [ 'void', [ 'pointer', 'int' ]],
  zitdev_set_en_1pps: [ 'void', [ 'pointer', 'int' ]],
});

const zitDevHandle = libZitDevAddon.zitdev_create('/dev/AxiGP0', Buffer.alloc(0));

function setPTTSwitch(isEn) {
  libZitDevAddon.zitdev_set_en_ptt(zitDevHandle, isEn);
}
// setPTTSwitch(0);

exports.setPTTSwitch = setPTTSwitch;
