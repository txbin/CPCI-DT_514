'use strict';

/*
 * @Description:窄带射频测量
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-24 15:25:23
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-10-23 14:28:22
 */

const ffi = require('ffi');

const addon = ffi.Library('libzitgpnb.so.1.0.0', {
  zitgpnb_get_pwr: [ 'double', []], // 发射功率
  zitgpnb_get_fqr: [ 'double', []], // 发射频点
  zitgpnb_jump_frq: [ 'int', [ 'pointer' ]], // 跳频频率
  zitgpnb_jump_spd: [ 'int', []], // 跳速
  zitgpnb_jump_pwr: [ 'double', []], // 跳频平均功率
});

module.exports = addon;
