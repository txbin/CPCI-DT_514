'use strict';
const ffi = require('ffi');
const ref = require('ref');
const ArrayType = require('ref-array');

const int = ref.types.int; // typedef
const IntArray = ArrayType(int); // define the "int[]" type
const libzitauad = ffi.Library('libzitauad.so.1', {
  zitauad_create: [ 'pointer', [ 'string', 'pointer' ]], // 创建采集对象
  zitauad_delete: [ 'void', [ 'pointer' ]], // 删除对象
  zitauad_set_en: [ 'void', [ 'pointer', 'int' ]], // 控制启停
  // zitauad_set_config: [ 'int', [ 'pointer', 'int' ]], // 采集配置
  zitauad_snapshot: [ 'void', [ 'pointer', IntArray, 'int' ]], // 采集快照数据
});

class AudioAddonAD {
  constructor(params = '/dev/AxiGP0', callback = Buffer.alloc(0)) {
    this._auadHandle = libzitauad.zitauad_create(params, callback);
    console.log(
      'TCL: AudioAddonAD -> constructor -> _auadHandle',
      this._auadHandle
    );
  }
  /**
   *采集信号
   *
   * @param {Buffer} bufferArry 数据存储数组
   * @memberof AudioAddon
   */
  snapshot(bufferArry) {

    libzitauad.zitauad_set_en(this._auadHandle, 1); // 启动采集
    libzitauad.zitauad_snapshot(
      this._auadHandle,
      bufferArry,
      bufferArry.length / 4
    ); // 采集数据
    libzitauad.zitauad_set_en(this._auadHandle, 0); // 停止采集
  }
}

// const b = new AudioAddonAD();
// console.log('TCL: b', b);

module.exports = AudioAddonAD;
