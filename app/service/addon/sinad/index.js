'use strict';
const AudioDA = require('../audioda');
const AudioAD = require('../audioad');


const algClass = require('./libzitalg');

const audioDAInstance = new AudioDA();
const audioADInstance = new AudioAD();

const AUDIO_BUFFER_LEN = 4096 * 4; // 音频采集buffer长度
const algClassInstance = new algClass(AUDIO_BUFFER_LEN / 4);

const tonePlay = (freq = 800, level = 210) => {
  audioDAInstance.setEn(1);
  audioDAInstance.setLevel(level);
  audioDAInstance.setFreq(freq);
};

const toneStop = () => {
  audioDAInstance.setEn(0);
};

const getSINAD = () => {

  let audioBuffer = Buffer.alloc(AUDIO_BUFFER_LEN);
  console.log('TCL: getSINAD -> audioBuffer.length', audioBuffer.length);
  audioADInstance.snapshot(audioBuffer);

  const sinadValue = algClassInstance.computeSinad(
    audioBuffer,
    AUDIO_BUFFER_LEN / 4
  );
  console.log('信纳比值TCL: getSINAD -> sinadValue', sinadValue);
  audioBuffer = null;
  return sinadValue;
};

/**
 *音频测量
 * @param {*} buffLen 音频采集点数长度
 *
 * @return  {Object} audioResult frequency：音频频率； millvolt：音频电压(毫伏)；sinad：音频信纳比；distortion音频失真度
 */
function getAudioTestResult(buffLen) {
  let audioBuffer = Buffer.alloc(buffLen * 4);
  console.log('TCL: getAudioTestResult -> audioBuffer', '1开始音频测量，buffLen：', buffLen);
  audioADInstance.snapshot(audioBuffer);
  console.log('TCL: audioBuffer.length', audioBuffer.length, '3音频采集完成结束，长度为：', buffLen);

  let audioResult = algClassInstance.computedAudioResult(audioBuffer, buffLen);
  console.log('TCL: audioResult 4 音频测量计算结果：', audioResult);
  audioBuffer = null;
  audioResult = audioResult ? audioResult : {};
  return audioResult;
}

// for (let i = 0; i < 100; i++) {
// console.log('TCL: i', i, new Date().getTime());
// getSINAD();
// setInterval(() => {
//   console.time('音频结果计算');
//   getAudioTestResult(4096);
//   console.timeEnd('音频结果计算');
// }, 5000);
// }

module.exports = {
  tonePlay,
  toneStop,
  getSINAD,
  getAudioTestResult,
};
