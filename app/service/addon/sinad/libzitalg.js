'use strict';
const ffi = require('ffi');
const ref = require('ref');
const ArrayType = require('ref-array');
const Struct = require('ref-struct');

// typedef
const int = ref.types.int;
const IntArray = ArrayType(int);

const libzitalg = ffi.Library('libzitalg.so.1.0.0', {
  zitalg_sinad_create: [ 'pointer', [ 'int', 'pointer' ]],
  zitalg_sinad_compute: [ 'double', [ 'pointer', IntArray, 'int' ]], // 计算信纳比
  // zitalg_evm: [ 'double', [ 'pointer', IntArray ]], // evm计算

  zitalg_asp_create: [ 'pointer', [ 'int' ]], // 音频测量
  zitalg_asp_compute: [ 'void', [ 'pointer', IntArray, 'int', 'pointer' ]],
  zitalg_asp_delete: [ 'void', [ 'pointer' ]],
});

class algClass {
  constructor(len) {
    this.HANDLE = libzitalg.zitalg_sinad_create(len, Buffer.alloc(0));
  }
  computeSinad(arry, len) {
    const sinad = libzitalg.zitalg_sinad_compute(this.HANDLE, arry, len);
    return sinad;
  }

  computedAudioResult(audioBuff, len) {
    if (!len || len <= 0) return console.error('音频测量数据长度错误');

    const audio_struct = Struct({
      millivolt: 'double', // 音频电压，单位是毫伏                               */
      frequency: 'double', // 音频频率，单位是赫兹                               */
      sinad: 'double', // 音频信纳比，单位是dB                               */
      distortion: 'double', // 音频信纳比，单位是dB
    });

    const audio_result_struct = new audio_struct();

    const asp = libzitalg.zitalg_asp_create(len);
    libzitalg.zitalg_asp_compute(asp, audioBuff, len, audio_result_struct.ref());
    libzitalg.zitalg_asp_delete(asp);

    const millivolt = audio_result_struct.millivolt;
    const frequency = audio_result_struct.frequency;
    const sinad = audio_result_struct.sinad;
    const distortion = audio_result_struct.distortion;

    return { millivolt, frequency, sinad, distortion };
  }
}
module.exports = algClass;
