'use strict';

const Service = require('egg').Service;

const _rfConfig_PLAddon = require('../addon/rf_PL');
const narrowRFTest = require('../addon/rf_measure');
const sleep = require('../utils/tools').sleep;


class RfCheckService extends Service {
  /**
   * adda 射频自检
   *
   * @param {Object} { freq = 50, att1 = 0, att2 = 0 }
   * @return {Object} { dDetector, carrfqt, carrpower } 检波器功率 发射频率 发射功率
   * @memberof RfCheckService
   */
  async adda({ freq = 50, att1 = 0, att2 = 0 }) {
    // 1. 初始化发射
    // 2. 收发回环模式
    // 3. 发50M单音
    // 4. 2个发射内部衰减全零
    // 5. 检波器读值 ：算法判断DA。
    // 6. ADC收
    // 7. 算法查看结果，接收测试算法，比较收发频率值、功率值。
    _rfConfig_PLAddon.rf_tx_test_init();// 板子发射初始化
    _rfConfig_PLAddon.enRxTx(0); // 使能发射
    _rfConfig_PLAddon.setMode(0); // 设置单音
    _rfConfig_PLAddon.setFqt(freq); // 设置频率
    _rfConfig_PLAddon.att1(att1); // 发射衰减1
    _rfConfig_PLAddon.att2(att2); // 发射衰减2
    await sleep(1000);

    const dDetector = _rfConfig_PLAddon.getDetectors();// 检波器值

    // 测量发射频率
    let carrfqt = narrowRFTest.zitgpnb_get_fqr();
    if (carrfqt) carrfqt = Number((carrfqt / 1e6).toFixed(6));

    // 读取电台输出功率
    const carrpower = narrowRFTest.zitgpnb_get_pwr();

    return { dDetector, carrfqt, carrpower };
  }
}

module.exports = RfCheckService;
