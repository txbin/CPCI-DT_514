'use strict';

const Service = require('egg').Service;

const fixedMeasureLogic = require('./special/fixedMeasureLogic/index.js');

class GeneralTestNarrowService extends Service {
  async read_general_result_narrow(freq, shouldRxTest, shouldTxTest) {
    console.log('TCL: GeneralTest_narrowService -> read_general_result_narrow -> freq, shouldRxTest, shouldTxTest', freq, shouldRxTest, shouldTxTest);
    let txResult = {},
      rxResult = {};

    const { app, ctx } = this;
    const nsp = app.io.of('/');
    fixedMeasureLogic.eggCtxAssignment(ctx, nsp);

    if (shouldTxTest) txResult = await fixedMeasureLogic.txTest();
    if (shouldRxTest) rxResult = await fixedMeasureLogic.readRxResult(freq);

    return { ...txResult, ...rxResult };
  }
}

module.exports = GeneralTestNarrowService;
