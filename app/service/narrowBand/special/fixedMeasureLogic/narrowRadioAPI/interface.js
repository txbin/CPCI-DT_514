/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-21 17:02:18
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-10-23 09:19:58
 */
'use strict';
const assist = require('./base');
const _Data = require('./data');

const SerialportObj = require('./serialportObj');

// let _serialportObj = null;

let heartInterval;

const ffi = require('ffi');
const ref = require('ref');
const ComName = '/dev/ttyPS5';

let _serialportObj;

const _onDataListener = ffi.Callback('int', [ 'pointer', 'int' ], function(
  backPointer,
  len
) {
  const val = ref.reinterpret(backPointer, len, 0); // 读取C++中返回的Buffer数据，arg[2]是读取的起始地址

  const tempBuff = Buffer.concat(
    [ _serialportObj._hadRecBuffer, val ],
    _serialportObj._hadRecBuffer.length + val.length
  );
  _serialportObj._hadRecBuffer = tempBuff;
  console.log('TCL: _hadRecBuffer', _serialportObj._hadRecBuffer.length, val);
});

async function getInstance() {
  if (!_serialportObj) {
    _serialportObj = new SerialportObj(ComName, 115200, _onDataListener);
    setRC(0);
    RcService();

    await initConfig();
  }
}

async function initConfig() {
  console.log('<--------init config 171--------->');
  await setSwitch(17);
  // FIXME:需要设置为1小功率模式,信道1
  // await setChannel(2);
  // await setPower(1);
  const configObject = await getParams();
  if (configObject !== false) {
    configObject.waveform = 2;
    configObject.mode = 0;
    configObject.network = 1;
    await setParams(configObject);
  }
  await setVolume(1);
}
/**
 *结束维护心跳包循环,接口电台控制，并关闭串口
 *
 */
function stopRadioCtrl() {
  if (_serialportObj) {
    setRC(1);
    clearInterval(heartInterval);
    // console.log('关闭心跳TCL: stopRadioCtrl -> heartInterval');

    setTimeout(() => {
      _serialportObj.close();
      _serialportObj = null;
    }, 500);
  } else {
    console.error('串口关闭失败');
  }
}

/**
 * todo 设置遥控状态
 * @param {int} control  0：进入遥控状态    1：退出遥控状态
 * *return
 */
function setRC(control) {
  console.log('<==================================================>');
  const controlHex = parseInt(control, 16);
  const data = _Data.dataSetRc.concat(controlHex);
  const sendInfo = assist.getSlip(data);
  console.log('TCL: setRC -> sendInfo', sendInfo);
  _serialportObj.writeData(sendInfo);
  // console.log(_serialportObj._portHandle.isOpen);
}
/**
 * todo 维护串口服务
 */
async function RcService() {
  console.log('<==================================================>');
  const sendInfo = assist.getSlip(_Data.dataRcService);
  heartInterval = setInterval(() => {
    _serialportObj.writeData(sendInfo);
  }, 2000);
}

async function getPower() {
  console.log('<==================================================>');
  const sendInfo = assist.getSlip(_Data.dataGetPower);
  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecPower,
    5000
  );
  return assist.checkRecGetInfo(backInfo, 'getPower');
}

/**
 * todo 设置功率
 * @param {int} power
 * ? @power        0 ：值守  1 ：小功率 2 ：中功率  3 ：大功率
 * *return  成功返回功率true  失败返回false
 */
async function setPower(power) {
  console.log('<==================================================>');
  const powerHex = parseInt(power, 16);
  console.log(`setPower: ${powerHex}`);
  const data = _Data.dataSetPower.concat(powerHex);
  const sendInfo = assist.getSlip(data);
  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecACK,
    5000
  );
  return assist.checkRecSetInfo(backInfo, 'setPower');
}

/**
 * !无法使用 存在查询后会更改开关状态BUG
 * todo 查询开关状态
 * *return  成功返回开关状态{int} switch  失败返回false
 * ? @switch      0 ：静噪开，导频开  1 ：静噪开，导频关  16 ：静噪关，导频开  17 ：静噪关，导频关
 */
async function getSwitch() {
  console.log('<==================================================>');
  const sendInfo = assist.getSlip(_Data.dataGetSwitch);
  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecSwitch,
    5000
  );
  return assist.checkRecGetInfo(backInfo, 'getSwitch');
}

/**
 * todo 设置开关状态
 * @param {*} switchS
 * ? @switchS     0 ：静噪开，导频开  1 ：静噪开，导频关  16 ：静噪关，导频开  17 ：静噪关，导频关
 * *return  成功返回true  失败返回false
 */
async function setSwitch(switchS) {
  console.log('<==================================================>');
  const switchSHex = switchS;
  console.log(`setSwitch: ${switchSHex}`);
  const data = _Data.dataSetSwitch.concat(switchSHex);
  const sendInfo = assist.getSlip(data);
  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecACK,
    5000
  );
  return assist.checkRecSetInfo(backInfo, 'setSwitch');
}

/**
 * todo 查询信道号
 * *return  成功返回信道号{int} channel  失败返回false
 * ? @channel      0 ~ 19个信道
 */
async function getChannel() {
  console.log('<==================================================>');
  const sendInfo = assist.getSlip(_Data.dataGetChannel);
  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecChannel,
    5000
  );
  return assist.checkRecGetInfo(backInfo, 'getChannel');
}

/**
 * todo 设置信道号
 * @param {int} channel
 * ? @channel      0 ~ 19个信道
 * *return  成功返回true  失败返回false
 */
async function setChannel(channel) {
  console.log('<==================================================>');
  const channelHex = parseInt(channel, 16);
  console.log(`setChannel: ${channelHex}`);
  const data = _Data.dataSetChannel.concat(channelHex);
  const sendInfo = assist.getSlip(data);
  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecACK,
    5000
  );
  return assist.checkRecSetInfo(backInfo, 'setChannel');
}

/**
 * todo 设置音量
 * @param {int} volume
 * ? @volume      0-静音 1-耳语 2-音量1 3-音量2 4-音量3
 * *return  成功返回true  失败返回false
 */
async function setVolume(volume) {
  console.log('<==================================================>');
  const volumeHex = parseInt(volume, 16);
  const data = _Data.dataSetVolume.concat(volumeHex);
  const sendInfo = assist.getSlip(data);
  console.log({ sendInfo });

  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecACK,
    5000
  );
  return assist.checkRecSetInfo(backInfo, 'setVolume');
}

/**
 * todo 查询信道参数
 * *return  成功返回信道参数{Json} params  失败返回false
 * ? @params      Json对象
 */
async function getParams() {
  console.log('<=====================查询信道参数=============================>');
  const sendInfo = assist.getSlip(_Data.dataGetParams);
  const backInfo = await _serialportObj.cfgRadioParams(sendInfo, _Data.dataRecParams, 5000);
  return assist.sliceParams(backInfo);

}

/**
 * todo 设置信道参数
 * @param {Object} obj
 * ? @json      Json对象
 * *return  成 功返回true  失败返回false
 */
async function setParams(obj) {
  console.log('<==================================================>');
  console.log('123setParamsHex:', obj);
  const paramsHex = assist.concatParams(obj);
  const data = _Data.dataSetParams.concat(paramsHex);
  console.log('123sendInfo:', data);

  const sendInfo = assist.getSlip(data);
  // console.error('写入:', sendInfo)
  const backInfo = await _serialportObj.cfgRadioParams(
    sendInfo,
    _Data.dataRecACK,
    5000
  );
  return assist.checkRecSetInfo(backInfo, 'setParams');
}

module.exports = {
  getInstance,
  stopRadioCtrl,
  // setRC,
  // RcService,
  setPower,
  getPower,
  setChannel,
  getChannel,
  setParams,
  getParams,
  setSwitch,
  getSwitch,
  setVolume,
};
