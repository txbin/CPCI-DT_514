/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-21 17:02:18
 * @LastEditors: TangXB
 * @LastEditTime: 2019-09-05 16:02:22
 */
'use strict';
// 引入串口操作类
// import Serialport from 'serialport'
const Serialport = require('../../../../addon/serialport');


const sleepT = time => {
  return new Promise(resolve => setTimeout(resolve, time));
};

class SerialportObj {
  // 构造器
  constructor(port, baudrate, _onDataListener) {
    // 创建串口操作对象
    this._hadRecBuffer = Buffer.from([ ]);
    this._portHandle = new Serialport(port, baudrate, _onDataListener);

  }

  /**
   *查询，设置电台
   * todo 流程 1、发送信令 2、设置超时 3、创建监听事件 3、将监听到的data添加到SLIP待处理帧队列 队尾
   * todo 4、查看SLIP队头是不是0xc0帧头 否：清空队列 是：判断SLIP队尾是否是0xc0帧未
   * todo 5、否：等待监听到的下一个data 是： 每次取出对头的帧，判断是否为期望收到的帧。
   * @param {Buffer} cfgData 配置电台数据
   * @param {int} frameType 期望回读的帧类型
   * @param {Number} timeout  回读响应数据超时时间
   * @return {JSON} 配置后响应数据
   * @memberof SerialportObj
   */
  async cfgRadioParams(cfgData, frameType, timeout) {
    const _this = this;
    // console.log('TCL: SerialportObj -> cfgRadioParams -> this', this);
    _this._hadRecBuffer = Buffer.from([]); // 先将数据队列清空

    _this._portHandle.write(cfgData);
    // 获取配置响应内容

    const response = await isRecConfigResponse(timeout);
    return response;

    async function isRecConfigResponse(timeout) {
      const loopCount = 25;
      await sleepT(timeout / loopCount); // 最长等待直到超时
      for (let i = 0; i < loopCount; i++) {
        const resolveBuf = _this.sliceSlip(_this._hadRecBuffer);
        // console.log('TCL: SerialportObj -> isRecConfigResponse -> _hadRecBuffer', _this._hadRecBuffer);
        // console.log('TCL: SerialportObj -> isRecConfigResponse -> resolveBuf', resolveBuf);
        await sleepT(150);
        if (_this.parseFrameType(resolveBuf, frameType)) {
          // 收到想要的帧
          _this._hadRecBuffer = Buffer.from([]);
          return resolveBuf;
        }
      }
    }
  }

  sliceSlip(buflist) {
    if (!buflist || buflist.length < 2) return;
    let head = buflist.indexOf(0xc0);
    const end = buflist.indexOf(0xc0, head + 2);
    if (head === -1 || end === -1) return;

    if (buflist[head + 1] === 0xc0) head++;

    const buftemp = buflist.slice(head, end + 1);
    this._hadRecBuffer = buflist.slice(end + 1);
    return buftemp;
  }
  /**
   * todo 判断该类型的帧是否是期望收到的   比较信令ID
   * @param {Buffer} msg 收到Slip帧
   * @param {Array} FrameType 期望收到的用户帧
   */
  parseFrameType(msg, FrameType) {
    if (!msg) return;
    const frameType = Buffer.from(FrameType);
    const msgId = Buffer.alloc(2);
    msg.copy(msgId, 0, 1, 3);
    const frameTypeId = Buffer.alloc(2);
    frameType.copy(frameTypeId, 0, 0, 2);
    return msgId.equals(frameTypeId);
  }
  // 向串口写入数据
  writeData(data) {
    this._portHandle.write(data);
  }
  close() {
    this._portHandle.close();
  }
  sleep(time) {
    return new Promise(resolve => setTimeout(resolve, time));
  }
  getData() {
    return this._hadRecBuffer;
  }
}


module.exports = SerialportObj;
