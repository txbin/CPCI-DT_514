/* eslint-disable no-bitwise */
'use strict';

class Help {
  /**
   * todo 计算用户信令得到，16Bit CRC校验数组
   * @param {Array} data 用户信令
   * *return 返回大端模式CRC(hex)，Array
   */
  getCRC(data) {
    let i;
    let j;
    const len = data.length;
    let crc,
      flag;
    crc = 0x0000;
    for (i = 0; i < len; i++) {
      crc ^= data[i] << 8;
      for (j = 0; j < 8; j++) {
        flag = crc & 0x8000;
        crc <<= 1;
        if (flag) {
          crc &= 0xfffe;
          crc ^= 0x8005;
        }
      }
    }
    const hex = [];
    for (let i = 1; i >= 0; i--) {
      hex[i] = crc % 256 & 0xff;
      crc /= 256;
    }
    return hex;
  }

  /**
   * todo 将用户信令和CRC拼接成一个Array
   * @param {Array} param 用户信令
   * *返回这个Array
   */
  concatCRC(param) {
    const dataHead = param;
    const lenHead = dataHead.length;
    const CRC = this.getCRC(dataHead);
    let data = new Array(lenHead + 2);
    data = dataHead.concat(CRC);
    // console.log("userData: ", data);
    return data;
  }

  /**
   * todo 校验用户帧 CRC是否正确
   * @param {*} param
   * *return dataHead 返回去除CRC的用户帧
   */
  checkCRC(param) {
    const data = param;
    const len = data.length;
    const CRC = [ data[len - 2], data[len - 1] ];
    const dataHead = data.slice(0, len - 2);
    const countCRC = this.getCRC(dataHead);

    console.log('电台回包CRC校验,帧中CRC：', CRC, '计算CRC:', countCRC);

    if (CRC[1] === countCRC[1]) {
      console.log('checkCRC:', true);
      return dataHead;
    }
    console.error('checkCRC:', false);
    return false;
  }

  /**
   * todo 转义信令为 SLIP协议信令   C0 => DB DC     DB => DB DD  信令首位+C0
   * @param {Array} param 需要转义的Array
   * *return Buffer
   */
  transfer(param) {
    const data = param;
    let len = data.length;
    for (let i = 0; i < len; i++) {
      if (data[i] === 0xdb) {
        data.splice(i, 1, 0xdb, 0xdd);
        len++;
        i++;
      }
      if (data[i] === 0xc0) {
        data.splice(i, 1, 0xdb, 0xdc);
        len++;
        i++;
      }
    }
    data.splice(0, 0, 0xc0);
    data.splice(data.length, 0, 0xc0);
    const buf = Buffer.from(data);
    return buf;
  }

  /**
   * todo 用户帧 0xdb、0xdc转义0xc      0xdb、0xdd转义0xdb
   * @param {*} params 未转义用户帧
   * *return 返回用户帧（含CRC校验）
   */
  untransfer(params) {
    const data = params;
    let len = data.length;
    for (let i = 0; i < len; i++) {
      if (data[i] === 0xdb && data[i + 1] === 0xdc) {
        data.splice(i, 2, 0xc0);
        len--;
      }
      if (data[i] === 0xdb && data[i + 1] === 0xdd) {
        data.splice(i, 2, 0xdb);
        len--;
      }
    }
    // console.log("untransfer:", data);
    return data;
  }

  /**
   * todo 组合 concatCRC transfer
   * @param {Array} param 用户信息帧（无CRC校验）
   * *return Buffer
   */
  getSlip(param) {
    console.log('getslip', param);
    const data = this.concatCRC(param);
    const sendInfo = this.transfer(data);
    console.log('getslip return', param);
    return sendInfo;
  }

  /**
   * todo 拆分Slip帧
   * @param {Buffer} param Slip帧
   * *return {Array}用户信息帧（无CRC校验）
   */
  getUnSlip(param) {
    console.log('TCL: Help -> getUnSlip -> param', param);
    const datatemp = this.split(param);
    console.log('TCL: Help -> getUnSlip -> datatemp', datatemp.toString('hex'));
    const dataUser = this.untransfer(datatemp);
    console.log('TCL: Help -> getUnSlip -> dataUser', dataUser.toString('hex'));
    const dataHead = this.checkCRC(dataUser);
    console.log('TCL: Help -> getUnSlip -> dataHead', dataHead.toString('hex'));
    return dataHead;
  }
  /**
   * todo 拆分slip帧头、尾的0xc0
   * @param {Buffer} params 参数尾
   * *return {Array}无头尾标识0xC0的数组
   */
  split(params) {
    const dataSlip = [];
    for (const value of params.values()) {
      dataSlip.push(value);
    }
    const len = dataSlip.length;
    if (dataSlip[0] === 0xc0 && dataSlip[len - 1] === 0xc0) {
      dataSlip.splice(len - 1, 1);
      dataSlip.splice(0, 1);
    }
    return dataSlip;
  }
  /**
   * todo 将信道参数对象的属性的变量压入数组
   * @param {Object} obj 信道参数对象
   * *return 信道参数Array
   */
  concatParams(obj) {
    const {
      channel,
      waveform,
      mode,
      freq,
      keyM,
      keyS,
      fhTableNum,
      fhNetNum,
      bitRate,
      role,
      network,
    } = obj;
    console.log('mode解析:', mode, obj);

    const channelHex = parseInt(channel, 16);
    const waveformHex = parseInt(waveform, 16);
    const modeHex = parseInt(mode, 16);
    console.log('TCL: Help -> concatParams -> freq', freq);
    const freqInt = parseInt(freq);
    const freqlist1 = parseInt(freqInt / 100, 16);
    const freqlist2 = parseInt(freqInt % 100, 16);
    const freqlist3 =
    parseInt((((freq * 1000) % 1000) / 100) << 4) +
    parseInt(((parseInt(freq * 1000) % 100)) / 25);

    const keyMHex = parseInt(keyM, 16);
    const keySHex = parseInt(keyS, 16);
    const fhTableNumHex = parseInt(fhTableNum, 16);

    let fhNetNumlist1 = fhNetNum / 100;
    let fhNetNumlist2 = fhNetNum % 100;
    fhNetNumlist1 = parseInt(fhNetNumlist1, 16);
    fhNetNumlist2 = parseInt(fhNetNumlist2, 16);

    const bitRateHex = parseInt(bitRate, 16);
    const roleHex = parseInt(role, 16);
    const networkHex = parseInt(network, 16);
    const paramsHex = [
      channelHex,
      waveformHex,
      modeHex,
      freqlist1,
      freqlist2,
      freqlist3,
      keyMHex,
      keySHex,
      fhTableNumHex,
      fhNetNumlist1,
      fhNetNumlist2,
      bitRateHex,
      roleHex,
      networkHex,
    ];
    console.log('concat ****return ', paramsHex);
    return paramsHex;
  }

  /**
   * todo 拆分信道参数数组，并组成信道参数对象
   * @param {*} paramsHex 信道参数Buffer
   * *return {Object}信道参数对象
   */
  sliceParams(paramsHex) {
    if (Buffer.isBuffer(paramsHex)) {
      const dataHead = this.getUnSlip(paramsHex);
      if (!dataHead) console.error('******解析电台返回帧错误*******');

      const channel = this.BCDtoDec(dataHead[4]);
      const waveform = this.BCDtoDec(dataHead[5]);
      const mode = this.BCDtoDec(dataHead[6]);
      let freq = parseInt(dataHead[7]) * 100;
      freq += this.BCDtoDec(dataHead[8]);
      freq +=
        parseInt(this.BCDtoDec(dataHead[9]) / 10) / 10 +
        (parseInt(this.BCDtoDec(dataHead[9]) % 10) * 25) / 1000;
      const keyM = this.BCDtoDec(dataHead[10]);
      const keyS = this.BCDtoDec(dataHead[11]);
      const fhTableNum = this.BCDtoDec(dataHead[12]);
      let fhNetNum = this.BCDtoDec(dataHead[13]) * 100;
      fhNetNum += this.BCDtoDec(dataHead[14]);
      const bitRate = this.BCDtoDec(dataHead[15]);
      const role = this.BCDtoDec(dataHead[16]);
      const network = this.BCDtoDec(dataHead[17]);
      return {
        channel,
        waveform,
        mode,
        freq,
        keyM,
        keyS,
        fhTableNum,
        fhNetNum,
        bitRate,
        role,
        network,
      };
    }
    return false;

  }

  /**
   * todo BCD码转10进制
   * @param {int} bcd
   * *return BCD码十进制
   */
  BCDtoDec(bcd) {

    let dec = 0;
    const tmp = ((bcd >> 4) & 0x0f) * 10 + (bcd & 0x0f);
    dec += tmp;
    return dec;
  }

  /**
   * todo 检测查询操作backInfo类型Buffer，打印get操作结果
   * @param {Buffer} backInfo 接受到的值
   * @param {String} handleName 操作名称
   * *return 成功返回对应值 错误返回false
   */
  checkRecGetInfo(backInfo, handleName) {
    const name = handleName.slice(3);
    if (Buffer.isBuffer(backInfo)) {
      console.log(`${handleName}: `, true);
      const dataHead = this.getUnSlip(backInfo);
      console.log(`${name}: `, dataHead[4]);
      return dataHead[4];
    }
    console.log(`${handleName}: `, false);
    return false;

  }

  /**
   *todo 检测设置操作backInfo类型Buffer，打印该设置操作名称操作结果
   * @param {Buffer} backInfo 接受到的值
   * @param {String} handleName 操作名称
   * *
   */
  checkRecSetInfo(backInfo, handleName) {
    if (Buffer.isBuffer(backInfo)) {
      console.log(`${handleName}: `, true);
      return true;
    }
    console.log(`${handleName}: `, false);
    return false;

  }
}

module.exports = new Help();
