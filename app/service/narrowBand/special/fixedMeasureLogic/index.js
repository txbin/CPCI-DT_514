/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-21 17:02:18
 * @LastEditors  : TangXB
 * @LastEditTime : 2020-01-03 20:29:11
 */
'use strict';
let NSP;
const _radioAPI = require('./narrowRadioAPI/interface');
const sleep = require('../../../utils/tools').sleep;
const path = require('path');

// const _sinadAPI = require('./171API/SINAD/index');
// import { UDP_SOCKET } from "./node_modules/@/JS/instanceTool/EthernetUdpRec.js.js.js";
// import { U3IPs } from "./node_modules/@/JS/commData.js.js.js";
// const FPGA_DATA_171 = require('./udpData/fpgaData');
// import dataConvert from "./node_modules/@/JS/utils/dataConvert.js.js.js";

const narrowRFTest = require('../../../addon/rf_measure');
const findActualCarrPower = require('../../../utils/findPowerValue');

const reviseTableDir = 'app/public/reviseTable';
const nb_power_table_rx_file = path.join(reviseTableDir, 'nb_rx_revise_table.json');
const nb_power_table_tx_file = path.join(reviseTableDir, 'nb_tx_revise_table.json');

const _pttAddon = require('../../../addon/dev');
const _rfConfig_PLAddon = require('../../../addon/rf_PL'); // 射频配置
const _sinadAddon = require('../../../addon/sinad');

let app_ctx = null;

/**
 *设置电台信道参数（定频模式、频率）
 *
 * @param {Object} radioCfgParams {  channel, waveform, mode, freq, keyM, keyS, fhTableNum, fhNetNum, bitRate, role, network,}
 * @param {Number} powerFlag 0:值守，1：小功率，2：中功率，3：大功率
 * @return {Boolean} cfgSuc 是否配置成功
 */
async function cfgRadioModelFreq(radioCfgParams, powerFlag) {
  let cfgSuc = false;
  // 1、设置电台兼容模式（定频）
  await _radioAPI.getInstance();
  await _radioAPI.setPower(Math.trunc(powerFlag));

  // 2、设置电台频点
  let radioStateObj;
  for (let i = 0; i < 3; i++) {

    radioStateObj = await _radioAPI.getParams();
    console.log('1、读取电台参数:', radioStateObj);
    if (!radioStateObj) {
      console.warn(`由于获取电台参数为空，重试第${i}次`, radioStateObj);

      if (i === 2) {
        _radioAPI.stopRadioCtrl(); // 关闭电台控制服务
        app_ctx.throw(423, '尝试3次读取电台信息失败，请确认电台已正确连接，并手动更改一次电台发射频率后再次进行测试');
      }
      continue;
    }

    console.log('频率设置，获取电台参数:', radioStateObj);
    radioStateObj = Object.assign(radioStateObj, radioCfgParams);

    cfgSuc = await _radioAPI.setParams(radioStateObj);
    if (cfgSuc) break;
  }

  if (!cfgSuc) {
    _radioAPI.stopRadioCtrl(); // 关闭电台控制服务
    app_ctx.throw(423, '尝试3次设置电台参数失败,请确认电台正确连接，并手动更改一次电台发射频率后再次进行测试');
  }

  return cfgSuc;
}

/**
 *电台的发射测试（电台发）
 *
//  * @param {number} freqValue 发射频点
 * @return { Object } 发射测试结果
 *
 */
async function txTest() {
  NSP.emit('data', '1.1测试模块初始化');

  _rfConfig_PLAddon.rf_rx_test_init(); // 先初始化我方接收射频
  await sleep(1000);

  // 1、产生PTT控制
  console.log('1.2打开PTT');
  NSP.emit('data', '1.2打开PTT');
  await _pttAddon.setPTTSwitch(0);

  // 2、播放音频
  NSP.emit('data', '1.3开始音频播放');
  _sinadAddon.tonePlay();
  await sleep(3000);

  // const fqtSet = parseFloat(freqValue).toFixed(6);
  let txTestResult = { carrpower: '', carrfqt: '' };
  // 3 调fpga 测试发射信息
  // 4 读取电台输出频率
  console.log('1.4读取电台频率');
  NSP.emit('data', '1.4读取电台频率');
  let carrfqt = narrowRFTest.zitgpnb_get_fqr();
  if (carrfqt) carrfqt = Number((carrfqt / 1e6).toFixed(6));

  // 7-读取电台输出功率
  console.log('1.5读取电台输出功率');
  NSP.emit('data', '1.5读取电台输出功率');
  let carrpower = narrowRFTest.zitgpnb_get_pwr();
  console.log('输出功率链路读取db值为', carrpower);
  // const freqValue = Math.round(carrfqt);
  // if (freqValue) {
  //   carrpower = findActualCarrPower.getRxValue(carrpower, freqValue, nb_power_table_rx_file);
  // }
  carrpower = Number(carrpower);

  txTestResult = { carrfqt, carrpower };

  // 8 - 关闭1k音频
  _sinadAddon.toneStop(1);
  // 9 - 关闭PTT使能
  console.log(' 关闭PTT使能');
  await _pttAddon.setPTTSwitch(1);

  return txTestResult;
  // return {};
}

// txTest();

async function rxTest(freqValue) {
  const radioSinadResult = {
    sens: '-1',
  };
  // 1 设置u3频点与使能FM
  console.log(' 2.1 设置我方射频频点与使能FM');
  NSP.emit('data', '2.1 设置射频频点与使能FM');
  _rfConfig_PLAddon.rf_tx_test_init(); // 先初始化我方射频

  await setU3FqtAndEnFM(freqValue);

  // 2 获取电台能否正确接收信号
  console.log('2.2 获取电台能否正确接收信号');
  NSP.emit('data', '2.2 获取电台能否正确接收信号');
  // 3 接收测试
  const recResult = await readRxResult(freqValue);
  Object.assign(radioSinadResult, recResult);

  // 4 设置FPGA关闭FM信号
  console.log('4 设置FPGA关闭FM信号');
  _rfConfig_PLAddon.enRxTx(0);

  return radioSinadResult;
}

/**
 *获取接收测试结果（针对被测设备）
 *
 * @param {*} freqValue 测试频点
 */
async function readRxResult(freqValue) {
  let recResult = {};
  const audioResult = await setTxCarrPowerAndGetSinad(-60, freqValue);
  console.log('无衰减情况下信纳比值是否大于12', audioResult.sinad);

  // 3 获取接收灵敏度

  if (audioResult.sinad > 12) {
    // 如果电台接收有效，才判断接收灵敏度
    console.log('2.3 获取接收灵敏度');
    NSP.emit('data', '2.3获取接收灵敏度');
    recResult = await getRadioSensitivity(freqValue);

  } else {
    console.warn('无衰减情况下，信纳比小于12dB');
    recResult.msg = '无衰减情况下，信纳比小于12dB';
  }

  return recResult;
}

/**
 *设置FPGA输出频点 和设置为FM模式，并发送FM信号
 *
 * @param {*} freqValue 射频频点
 */
async function setU3FqtAndEnFM(freqValue) {
  const freq = freqValue;

  // 1 设置u3发射频率
  _rfConfig_PLAddon.setFqt(freq);

  // 2 使能FM信号
  // --2.1设置为FM调制模式
  _rfConfig_PLAddon.setMode(1);
  // --2.2设置FPGA发射使能
  _rfConfig_PLAddon.enRxTx(3);
}

async function getRadioSensitivity(freqValue) {
  // debugger
  // 1 循环衰减输出功率 120 ：衰减120db
  const min = 120;
  const max = 60;
  let sens = -60;
  const audioResultAndSinad = {};
  for (let i = min; i >= max; i--) {
    NSP.emit('data', `2.4开始设置衰减值为：${i}的信纳比测试`);
    console.log(`信纳比测试，衰减：${i}`);
    await sleep(200);

    const audioResult = await setTxCarrPowerAndGetSinad(0 - i, freqValue);
    if (audioResult.sinad > 12) {
      sens = -i;
      Object.assign(audioResultAndSinad, audioResult, { sens });
      break;
    }
  }
  return audioResultAndSinad;
}

async function setTxCarrPowerAndGetSinad(carrpower, freqValue) {
  // 1 设置U3输出功率
  await setFPGATxCarrPower(carrpower, freqValue);
  await sleep(400);

  // 2 获取信纳比是否大于12dB
  console.time('音频结果计算');
  const audioResult = await _sinadAddon.getAudioTestResult(32768);
  console.timeEnd('音频结果计算');
  // let greaterCount = 0;
  // for (let index = 0; index < 3; index++) {
  //   // 信纳比值如果大于12 则测试三次，两次符合条件则返回
  //   await sleep(50);
  //   audioResult = await _sinadAddon.getAudioTestResult();
  //   const sinadValue = audioResult.sinad;
  //   const isGreaterThan12 = sinadValue ? sinadValue > 12 : false;
  //   console.log('信纳比值：是否大于12dB', isGreaterThan12, sinadValue);
  //   if (isGreaterThan12) greaterCount++;
  //   if (greaterCount > 1) {
  //     break;
  //   }
  // }

  return audioResult;
}
/**
 *设置FPGA输出功率值
 *
 * @param {*} carrpower 发射功率
 * @param {*} freqValue 发射频点
 */
async function setFPGATxCarrPower(carrpower, freqValue) {
  console.log('TCL: setFPGATxCarrPower -> freqValue', freqValue);
  // carrpower = findActualCarrPower.getTxValue(carrpower, freqValue, nb_power_table_tx_file); // 改为配置信号调理板卡，具体指为通过查表方式找校准后的值
  // console.log('设置发射衰减：', carrpower);

  // if (!carrpower) app_ctx.throw(423, `查找发射功率校准表错误,carrpower:${carrpower},范围为-120~-60。 freqValue:${freqValue}`);

  // const { inAttGn, inAttTx, digAtt } = carrpower;
  // console.log('TCL: setFPGATxCarrPower -> carrpower', carrpower);

  _rfConfig_PLAddon.setTxPower(carrpower);
}

/**
 * 171 电台测试
 *
 * @param {number} freq  freq 测试频点
 * @param {number} power  power 电台功率
 * @param {Boolean} isCheckRx  接收机测试
 * @param {Boolean} isCheckTx  发射机测试
 * @param {Object} ctx  app ctx对象
 * @param {Object} nsp  app nsp
 * @return {Object} { carrpower:0, fqt;0, fqr:0, sens:0 ,txBand:'error'} 发射功率，发射频率，接收频点，接收灵敏度
 */
const getResult = async (freq, power, isCheckRx, isCheckTx, ctx, nsp) => {
  const obj = {};
  eggCtxAssignment(ctx, nsp);
  NSP.emit('data', '开始17x定频窄带测试');

  freq = parseFloat(freq).toFixed(3);
  console.log('TCL: getResult -> freq', freq);
  // 1、设置电台信息
  await cfgRadioModelFreq({ freq }, power);
  await sleep(500);

  // return { data: 1 };

  if (isCheckTx) {
    console.log('电台发射测试', isCheckRx);
    const txResult = await txTest();
    Object.assign(obj, txResult);
  }

  if (isCheckRx) {
    console.log('电台接收测试', isCheckRx);
    const rxResult = await rxTest(freq);
    console.log('电台接收测试结果：', rxResult);
    Object.assign(obj, rxResult);
  }
  _radioAPI.stopRadioCtrl(); // 关闭电台控制服务

  return obj;
};
function eggCtxAssignment(ctx, nsp) {
  app_ctx = ctx;

  NSP = nsp;
}

module.exports = {
  getResult,
  setFPGATxCarrPower,
  rxTest,
  txTest, // 用于通用测量
  cfgRadioModelFreq,
  readRxResult, // 读取接收结果，用于通用测量
  eggCtxAssignment, // ctx和NSP赋初值
  _sinadAddon, // 音频播放器
  _pttAddon,
};
