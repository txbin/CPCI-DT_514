'use strict';

const Service = require('egg').Service;
const fixedMeasureLogic = require('./fixedMeasureLogic');
const hopMeasureLogic = require('./hopMeasureLogic');


class NarrowTestService extends Service {
  async fixed(freq, power, shouldRxTest, shouldTxTest) {
    const { app, ctx } = this;
    const nsp = app.io.of('/');

    return await fixedMeasureLogic.getResult(freq, power, shouldRxTest, shouldTxTest, ctx, nsp);
  }
  /**
   *跳频测量
   *
   * @return {object} object
   * @memberof NarrowTestService
   */
  async hop() {
    const { ctx } = this;
    const { power } = ctx.query;
    return await hopMeasureLogic.getResult({ power }, ctx);
    // return await nbMeasureLogic.getResult();
  }
}

module.exports = NarrowTestService;
