/*
 * @Description:
 * @version:
 * @Author: tangxiaobin
 * @Date: 2019-07-25 10:06:43
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-11-08 11:24:04
 */
'use strict';
const _pttAddon = require('../../../addon/dev');
const nbRadioCfgTools = require('./nbRadioCfgTools');
const narrowRFTest = require('../../../addon/rf_measure');
const sleep = require('../../../utils/tools').sleep;

const fixedMeasureLogic = require('../fixedMeasureLogic/index');
const _rfConfig_PLAddon = require('../../../addon/rf_PL');

const path = require('path');
const reviseTableDir = 'app/public/reviseTable';
const nb_power_table_rx_file = path.join(reviseTableDir, 'nb_rx_revise_table.json');
const findActualCarrPower = require('../../../utils/findPowerValue');


// const narrowHopMeasure = require('../../../addon/hopmeasure');


/**
 *
 *设置电台为跳频模式
 * @param {Number} power 设置电台功率值
 * @param {*} ctx_app app context
 */
const setRadioHopMode = async function(power, ctx_app) {
  const setParams = {};
  setParams.waveform = 2;
  setParams.mode = 5;

  await nbRadioCfgTools.cfgRadioModelFreq(setParams, ctx_app);

  await nbRadioCfgTools._radioAPI.setPower(power);
};

/**
 *读取跳频测量结果
 *
 */
const readHopResult = async function() {
  const hopInfo = {};


  hopInfo.hop = narrowRFTest.zitgpnb_jump_spd();
  const reportPower = narrowRFTest.zitgpnb_jump_pwr();
  const freqValue = 30;
  hopInfo.pwr = findActualCarrPower.getRxValue(reportPower, freqValue, nb_power_table_rx_file);


  const fqtBufferTemp = Buffer.alloc(1e4 * 4);
  const fqtBuffLen = narrowRFTest.zitgpnb_jump_frq(fqtBufferTemp);
  // // console.log('TCL: fqtBufferTemp', fqtBufferTemp);

  hopInfo.fqt = [];
  if (fqtBuffLen >= 1000) {
    hopInfo.msg = '跳频频率长度错误，长度大于1000';
    return hopInfo;
  }
  for (let i = 0; i < fqtBuffLen - 1; i++) {
    const freq = fqtBufferTemp.readUInt32LE(i * 4) / 1e6;
    console.log('TCL: freq', freq);
    hopInfo.fqt.push(Number(freq.toFixed(3)));
  }
  return hopInfo;
};

async function getResult({ power }, ctx_app) {
  let result = {};
  // 0、设置CPCI为接收模式
  const { app, ctx } = ctx_app;
  const nsp = app.io.of('/');
  fixedMeasureLogic.eggCtxAssignment(ctx, nsp);

  _rfConfig_PLAddon.rf_rx_test_init(); // 先初始化我方接收射频

  // 1、 设置跳频模式
  await setRadioHopMode(power, ctx_app);

  // 2、产生PTT控制
  console.log('打开PTT' + (new Date()).toLocaleTimeString());
  await _pttAddon.setPTTSwitch(0);
  await sleep(10000);

  // 3、 读取跳频测试结果
  console.log('TCL: getResult -> 读取跳频测试结果' + (new Date()).toLocaleTimeString());
  result = await readHopResult();
  // // console.log('TCL: getResult -> result', result);

  // 4、产生PTT控制
  console.log('关闭PTT' + (new Date()).toLocaleTimeString());
  console.log('TCL: getResult -> 关闭PTT');
  await _pttAddon.setPTTSwitch(1);

  // 5 、关闭电台控制
  console.log('TCL: getResult -> 关闭电台控制');
  nbRadioCfgTools.stopRadioCtrl(ctx_app);


  return result;
}
module.exports = {
  getResult,
};
