'use strict';
const _radioAPI = require('../fixedMeasureLogic/narrowRadioAPI/interface');

/**
 *设置电台信道参数（定频模式、频率）
 *
 * @param {Object} radioCfgParams {  channel, waveform, mode, freq, keyM, keyS, fhTableNum, fhNetNum, bitRate, role, network,}
 * @param {Object} app_ctx content 对象
 * @return {Boolean} cfgSuc 是否配置成功
 */
async function cfgRadioModelFreq(radioCfgParams, app_ctx) {
  let cfgSuc = false;
  // 1、设置电台兼容模式（定频）
  await _radioAPI.getInstance();

  // 2、设置电台频点
  let radioStateObj;
  for (let i = 0; i < 3; i++) {

    radioStateObj = await _radioAPI.getParams();
    console.log('1、读取电台参数:', radioStateObj);
    if (!radioStateObj) {
      console.warn(`由于获取电台参数为空，重试第${i}次`, radioStateObj);

      if (i === 2) {
        _radioAPI.stopRadioCtrl(); // 关闭电台控制服务
        app_ctx.throw(423, '尝试3次读取电台信息失败，请确认电台已正确连接并请手动设置一次电台频率值后再次进行测试');
      }
      continue;
    }

    console.log('频率设置，获取电台参数:', radioStateObj);
    radioStateObj = Object.assign(radioStateObj, radioCfgParams);

    cfgSuc = await _radioAPI.setParams(radioStateObj);
    if (cfgSuc) break;
  }
}
function stopRadioCtrl() {
  _radioAPI.stopRadioCtrl(); // 关闭电台控制服务
}

module.exports = {
  cfgRadioModelFreq,
  stopRadioCtrl,
  _radioAPI,
};
