/*
 * @Description:用于演示控制功能，将配置功能单独提取出来
 * @version:
 * @Author: TangXB
 * @Date: 2019-09-03 14:57:05
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-11-08 11:22:52
 */
'use strict';

const Service = require('egg').Service;
const _rfConfig_PLAddon = require('../addon/rf_PL');
const fixedMeasureLogic = require('./special/fixedMeasureLogic/index.js');

class DemoService extends Service {
  async setRFMode() {
    const { ctx } = this;
    const mode = Number(ctx.query.rfMode);
    console.log('设置射频模式（ 1:fm,0:单音）：', mode);
    _rfConfig_PLAddon.setMode(mode); // 1:fm,0:单音
  }

  async setAttx() {
    const { ctx } = this;
    const attx = Number(ctx.query.attx);
    const fqt = Number(ctx.query.freqTx);
    console.log('TCL: DemoService -> setAttx -> attx', attx);

    fixedMeasureLogic.setFPGATxCarrPower(attx, fqt); //
  }
  async setFqt() {
    const { ctx } = this;
    const fqt = Number(ctx.query.fqt);
    console.log('TCL: DemoService -> setFqt -> fqt', fqt);

    _rfConfig_PLAddon.setFqt(fqt); //
  }
  async setFqr() {
    const { ctx } = this;
    const fqr = Number(ctx.query.fqr);
    console.log('TCL: DemoService -> setFqr -> fqr', fqr);

    _rfConfig_PLAddon.setFqr(fqr); //
  }
  async setPowerChannel() {
    const { ctx } = this;
    const channel = Number(ctx.query.channel);
    console.log('TCL: DemoService -> setPowerChannel -> channel', channel);

    _rfConfig_PLAddon.setPowerChannel(channel); // 0:小信号，1：大信号
  }
  async enRxTx() {
    const { ctx } = this;
    const entxrx = Number(ctx.query.entxrx);

    _rfConfig_PLAddon.enRxTx(entxrx);
  }
  async rf_tx_test_init() {
    _rfConfig_PLAddon.rf_tx_test_init();
  }
  async rf_rx_test_init() {
    _rfConfig_PLAddon.rf_rx_test_init();
  }
  async att1() {
    const { ctx } = this;
    console.log('TCL: DemoService -> ctx', ctx.query);
    const attx1 = Number(ctx.query.attx1);
    _rfConfig_PLAddon.att1(attx1);
  }
  async att2() {
    const { ctx } = this;
    const attx2 = Number(ctx.query.attx2);
    _rfConfig_PLAddon.att2(attx2);
    console.log('TCL: DemoService -> _rfConfig_PLAddon.att2', _rfConfig_PLAddon.att2, attx2);
  }
  async attxDigit() {
    const { ctx } = this;
    const digitAttx = Number(ctx.query.digitAttx);
    _rfConfig_PLAddon.digitAttx(digitAttx);
  }
  async rxTest() {
    const { ctx } = this;
    const freq = Number(ctx.query.freq);

    const result = await fixedMeasureLogic.rxTest(freq);
    return result;
  }
  async txTest() {
    const result = await fixedMeasureLogic.txTest();
    return result;
  }
  async setGnr() {
    const { ctx } = this;
    const gnr = Number(ctx.query.gnr);

    const result = await _rfConfig_PLAddon.gnRx(gnr);
    return result;
  }
  async tonePlay() {
    const { ctx } = this;
    const freq = Number(ctx.query.freq);
    const level = Number(ctx.query.level);
    const en = Number(ctx.query.tonePlay);

    if (en === 0) await fixedMeasureLogic._sinadAddon.toneStop();
    else await fixedMeasureLogic._sinadAddon.tonePlay(freq, level);

    return 0;
  }
  async enPTT() {
    const { ctx } = this;
    const ptt = Number(ctx.query.ptt);

    await fixedMeasureLogic._pttAddon.setPTTSwitch(ptt);

    return 0;
  }
}

module.exports = DemoService;
