'use strict';

const Controller = require('egg').Controller;

class HelloController extends Controller {
  async connection(ws, req) {
    ws.uuid = this.service.registerUUID(req.connection.remoteAddress);
  }

  async message(ws, message) {
    this.service.handle(ws.uuid, message);
    ws.send(`receive ${message} by ${ws.uuid}`);
  }

  async close(ws, code) {
    console.log(`${ws.uuid} closed ${code}`);
    this.service.destroy(ws.uuid);
  }
}

module.exports = HelloController;