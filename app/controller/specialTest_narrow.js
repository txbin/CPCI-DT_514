/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-20 09:25:39
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-11-07 15:13:00
 */
'use strict';

const Controller = require('egg').Controller;
const validateDict = [ 'rx', 'tx' ];

const queryRule_Fixed = {
  freq: { type: 'number', min: 30, max: 520 }, // 测试频点范围
  power: [ 1, 2, 3 ], // 设置的电台功率范围值
};
const queryRule_Hop = {
  power: [ 1, 2, 3 ], // 设置的电台功率范围值
};
class SpecialTest_narrowController extends Controller {
  async fixedMeasuring() {
    const { ctx, app } = this;

    const freq = Number(ctx.query.freq);
    const power = Number(ctx.query.power);
    ctx.query.freq = freq;
    ctx.query.power = power;
    const errs = app.validator.validate(queryRule_Fixed, ctx.query);
    if (errs) ctx.throw(422, `字段参数${errs[0].field}设置错误：${errs[0].message}`);
    // ctx.validate(queryRule_Fixed, ctx.query);

    const testItem = ctx.query.params;
    if (!testItem) ctx.throw(422, `请设置测试对象, params:${validateDict}`);

    const queryArr = ctx.query.params.split(',') || []; // 获取请求测试数组,判断收、发测试
    const shouldRxTest = queryArr.indexOf(validateDict[0]) !== -1;
    const shouldTxTest = queryArr.indexOf(validateDict[1]) !== -1;

    const res = await ctx.service.narrowBand.special.narrowTest.fixed(
      freq,
      power,
      shouldRxTest,
      shouldTxTest
    );

    ctx.helper.success(ctx, res);
  }

  async hopMeasuring() {
    const { ctx, app } = this;

    const power = Number(ctx.query.power);
    ctx.query.power = power;
    const errs = app.validator.validate(queryRule_Hop, ctx.query);
    if (errs) ctx.throw(422, `字段参数${errs[0].field}设置错误：${errs[0].message}`);

    const res = await ctx.service.narrowBand.special.narrowTest.hop();
    ctx.helper.success(ctx, res);
  }
}

module.exports = SpecialTest_narrowController;
