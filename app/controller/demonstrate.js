/*
 * @Description:
 * @version:
 * @Author: tangxiaobin
 * @Date: 2019-09-03 11:39:44
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-10-11 14:14:51
 */
'use strict';

const Controller = require('egg').Controller;

// const fixedMeasureLogic = require('./service/narrowBand/special/fixedMeasureLogic/index.js');


class DemonstrateController extends Controller {
  async demoCfg() {
    const { ctx } = this;
    const query = this.ctx.query;
    let res = {};

    const reqKeys = Object.keys(query);
    console.log('TCL: DemonstrateController -> demoCfg -> reqKeys', reqKeys);

    if (reqKeys.indexOf('rfMode') > -1) {
      ctx.service.narrowBand.demo.setRFMode();
    } else if (reqKeys.indexOf('fqt') > -1) {
      ctx.service.narrowBand.demo.setFqt();
    } else if (reqKeys.indexOf('attx') > -1) {
      ctx.service.narrowBand.demo.setAttx();
    } else if (reqKeys.indexOf('attx1') > -1) { // 发射衰减1
      ctx.service.narrowBand.demo.att1();
    } else if (reqKeys.indexOf('attx2') > -1) { // 发射衰减2
      ctx.service.narrowBand.demo.att2();
    } else if (reqKeys.indexOf('digitAttx') > -1) { // 发射数字衰减
      ctx.service.narrowBand.demo.attxDigit();
    } else if (reqKeys.indexOf('fqr') > -1) {
      ctx.service.narrowBand.demo.setFqr();
    } else if (reqKeys.indexOf('gnr') > -1) {
      ctx.service.narrowBand.demo.setGnr();
    } else if (reqKeys.indexOf('channel') > -1) {
      ctx.service.narrowBand.demo.setPowerChannel();
    } else if (reqKeys.indexOf('init_rx') > -1) {
      ctx.service.narrowBand.demo.rf_rx_test_init();
    } else if (reqKeys.indexOf('init_tx') > -1) {
      ctx.service.narrowBand.demo.rf_tx_test_init();
    } else if (reqKeys.indexOf('entxrx') > -1) {
      ctx.service.narrowBand.demo.enRxTx();
    } else if (reqKeys.indexOf('rxTest') > -1) { // 17x接收直测
      res = await ctx.service.narrowBand.demo.rxTest();
    } else if (reqKeys.indexOf('txTest') > -1) { // 17x 发射直测
      res = await ctx.service.narrowBand.demo.txTest();
    } else if (reqKeys.indexOf('tonePlay') > -1) { // 17x音频播放
      res = await ctx.service.narrowBand.demo.tonePlay();
    } else if (reqKeys.indexOf('ptt') > -1) { // 17x音频播放
      res = await ctx.service.narrowBand.demo.enPTT();
    } else {
      console.error('未知参数值' + JSON.stringify(query));
      ctx.throw(422, '未知参数值' + JSON.stringify(query));
    }
    res = Object.assign(res, query);
    // console.log('TCL: DemoService -> demoCfg -> reqObj', reqObj);

    ctx.helper.success(ctx, res);
  }

}

module.exports = DemonstrateController;
