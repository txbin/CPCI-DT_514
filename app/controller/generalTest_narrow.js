/*
 * @Description:通用测试,窄带通用发射机、接收机测试
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-20 09:25:39
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-10-09 09:08:27
 */
'use strict';

const Controller = require('egg').Controller;
const validateDict = [ 'rx', 'tx' ];

const queryRule = {
  freq: { type: 'number', min: 30, max: 87.975 }, // 测试频点范围
};

class GeneralTest_narrowController extends Controller {
  async generalTest_narrow() {
    const { ctx } = this;

    const freq = Number(ctx.query.freq);
    ctx.query.freq = freq;
    ctx.validate(queryRule, ctx.query);

    const testItem = ctx.query.params;
    // ctx.helper.success(ctx, testItem);

    if (!testItem) ctx.throw(422, `请设置测试对象, params:${validateDict}`);

    const queryArr = ctx.query.params.split(',') || []; // 获取请求测试数组,判断收、发测试
    const shouldRxTest = queryArr.indexOf(validateDict[0]) !== -1;
    const shouldTxTest = queryArr.indexOf(validateDict[1]) !== -1;

    console.log('TCL: GeneralTest_narrowController -> generalTest_narrow -> ctx.service.narrowBand.generalTestNarrow', ctx.service.narrowBand.generalTestNarrow);
    const res = await ctx.service.narrowBand.generalTestNarrow.read_general_result_narrow(
      freq,
      shouldRxTest,
      shouldTxTest
    );

    ctx.helper.success(ctx, res);
  }

}

module.exports = GeneralTest_narrowController;
