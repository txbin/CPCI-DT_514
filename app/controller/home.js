/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-19 15:50:02
 * @LastEditors: TangXB
 * @LastEditTime: 2019-09-16 16:57:05
 */
'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    // ctx.body = 'hi, egg';
    await ctx.render('index');
  }
  async indexWs() {
    const { ctx } = this;
    // ctx.body = 'hi, egg';
    await ctx.render('index_ws');
  }
  async getSocketMsg() {
    const { app, ctx } = this;
    const WS = app.io.of('/');
    WS.emit('data', { name: 'tang', age: 66 });
    ctx.body = '我是服务端发送websocket消息';
    // ctx.body = 'hi, egg';
    // await ctx.render('index');
  }
}

module.exports = HomeController;
