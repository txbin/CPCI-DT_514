
'use strict';
/** 设备自检
 *
 *
*/

const Controller = require('egg').Controller;

const queryParamsRule = {
  adda: {
    freq: { type: 'number', required: false, min: 30, max: 6000 }, // 测试频点范围
    att1: { type: 'number', required: false, min: 0, max: 31.5 }, // 衰减1
    att2: { type: 'number', required: false, min: 0, max: 31.5 }, // 衰减2
  },
};

class SelfCheckController extends Controller {
  async adda() {
    const { ctx, app } = this;

    // for (const key in ctx.query) {
    //   ctx.query[key] = Number(ctx.query[key]);
    // }
    const errs = app.validator.validate(queryParamsRule.adda, ctx.query);
    if (errs) ctx.throw(422, `字段参数${errs[0].field}设置错误：${errs[0].message}`);

    // { freq = 50, att1 = 0, att2 = 0; }
    const res = await ctx.service.selfCheck.rfCheck.adda(ctx.query);
    ctx.helper.success(ctx, res);
  }
}

module.exports = SelfCheckController;
