/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-08-20 16:55:29
 * @LastEditors: TangXB
 * @LastEditTime: 2019-08-23 10:26:24
 */
'use strict';

const Controller = require('egg').Controller;
const path = require('path');
const fs = require('fs');
// 故名思意 异步二进制 写入流
const awaitWriteStream = require('await-stream-ready').write;
// 管道读入一个虫洞。
const sendToWormhole = require('stream-wormhole');

const reviseTableDir = 'app/public/reviseTable';
const nb_power_table_rx_file = path.join(reviseTableDir, 'nb_rx_revise_table.json');
const nb_power_table_tx_file = path.join(reviseTableDir, 'nb_tx_revise_table.json');


const writeFile = async function(targetPath, stream) {
  console.log('TCL: targetPath', targetPath);
  const writeStream = fs.createWriteStream(targetPath);
  console.log('-----------获取表单中其它数据 start--------------');
  console.log(`接收文件：${stream.filename}`);
  console.log('-----------获取表单中其它数据 end--------------');
  try {
    // 异步把文件流 写入
    await awaitWriteStream(stream.pipe(writeStream));
  } catch (err) {
    // 如果出现错误，关闭管道
    await sendToWormhole(stream);
    // 自定义方法
    // this.error(err);
    console.log('TCL: UploadJsonController -> Upload_fileController -> err', err);
    this.ctx.throw(424, err);
  }
};

class UploadJsonController extends Controller {
  async Upload_file_tx() {
    // 获取文件流
    const stream = await this.ctx.getFileStream();

    // 目标文件
    const target = nb_power_table_tx_file;
    await writeFile(target, stream);

    this.ctx.body = {
      name: stream.filename,
      // 所有表单字段都能通过 `stream.fields` 获取到
      fields: stream.fields,
    };
  }

  async Upload_file_rx() {
    // 获取文件流
    const stream = await this.ctx.getFileStream();

    // 目标文件
    const target = nb_power_table_rx_file;
    await writeFile(target, stream);

    this.ctx.body = {
      name: stream.filename,
      // 所有表单字段都能通过 `stream.fields` 获取到
      fields: stream.fields,
    };
  }
}


module.exports = UploadJsonController;
