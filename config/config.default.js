/*
 * @Description:
 * @version:
 * @Author: TangXB
 * @Date: 2019-06-19 15:50:02
 * @LastEditors: tangxiaobin
 * @LastEditTime: 2019-11-08 17:27:48
 */
/* eslint valid-jsdoc: "off" */

'use strict';
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {
    view: {
      // defaultViewEngine: 'nunjucks',
      mapping: {
        '.html': 'ejs',
      },
    },
    static: {
      maxAge: 31536000,
    },
    security: {
      csrf: {
        enable: false,
      },
    },
  });
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1560930557267_1620';

  // add your middleware config here
  config.middleware = [ 'processReqStart', 'errorHandler' ];

  config.processReqStart = {
    // 设置测试请求限制标志,同一时刻内只运行一个测试请求
    match: [ '/17x' ],
    isProcessReq: false,
  };

  config.multipart = {
    fileSize: '50mb',
    mode: 'stream',
    fileExtensions: [ '.json', '.txt' ], // 扩展几种上传的文件格式
  };

  config.cluster = {
    listen: {
      path: '',
      port: 9527,
      hostname: '0.0.0.0',
    },
  };
  config.io = {
    io: {
      init: {}, // passed to engine.io
      upgradeTimeout: 30000,
      namespace: {
        '/': {
          connectionMiddleware: [],
          packetMiddleware: [],
        },
        '/example': {
          connectionMiddleware: [],
          packetMiddleware: [],
        },
      },
    },
  };
  config.validate = {
    convert: true, // validate 收到参数后先对参数类型进行转换
    // validateRoot: false,
  };
  // add your user config here
  const userConfig = {
    myAppName: 'CPCI_DT_SERVER',
  };

  return {
    ...config,
    ...userConfig,
  };
};
